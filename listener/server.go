package listener

import (
	"context"
	"fmt"
	"io"
	"net"
	"os"
	"sync"
	"time"

	"google.golang.org/grpc"

	"github.com/armon/go-proxyproto"
	"github.com/grpc-ecosystem/grpc-opentracing/go/otgrpc"
	"github.com/sirupsen/logrus"
	broker "github.com/vx-labs/iot-mqtt-broker/api"
	api "github.com/vx-labs/iot-mqtt-broker/listener"
	"github.com/vx-labs/iot-mqtt-tcp-listener/tracing"
)

type Listener struct {
	listener   net.Listener
	logger     *logrus.Entry
	routines   sync.WaitGroup
	broker     *broker.Client
	brokerConn io.Closer
}

func NewListener(ctx context.Context) *Listener {
	l := &Listener{
		logger:   logrus.WithField("source", "listener"),
		routines: sync.WaitGroup{},
	}

	l.connectToBroker()
	return l
}

func (l *Listener) connectToBroker() error {
	tracer := tracing.Instance()
	conn, err := grpc.Dial(os.Getenv("BROKER_HOST"),
		append(broker.DefaultGRPCOpts(), grpc.WithUnaryInterceptor(
			otgrpc.OpenTracingClientInterceptor(tracer),
		),
			grpc.WithStreamInterceptor(
				otgrpc.OpenTracingStreamClientInterceptor(tracer),
			))...)
	if err != nil {
		return err
	}
	l.brokerConn = conn
	l.broker = broker.NewClient(conn)
	return nil
}

func (h *Listener) Close() error {
	err := h.listener.Close()
	if err != nil {
		return err
	}
	h.routines.Wait()
	logrus.Infof("listener stopped")
	return nil
}

func (h *Listener) StartTCP(port int) {
	l, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		logrus.Fatalln(err)
	}
	h.listener = l
	logrus.Infof("server is running on :%d", port)
	h.routines.Add(1)
	proxyList := &proxyproto.Listener{Listener: l}
	h.connManager(proxyList)
}

func (h *Listener) connManager(l net.Listener) {
	defer func() {
		h.routines.Done()
		logrus.Infof("mqtt listener stopped")
	}()
	var tempDelay time.Duration
	for {
		c, err := l.Accept()
		if err != nil {
			if err.Error() == fmt.Sprintf("accept tcp %v: use of closed network connection", l.Addr()) {
				return
			}
			if ne, ok := err.(net.Error); ok && ne.Temporary() {
				if tempDelay == 0 {
					tempDelay = 5 * time.Millisecond
				} else {
					tempDelay *= 2
				}
				if max := 1 * time.Second; tempDelay > max {
					tempDelay = max
				}
				logrus.Errorf("accept error: %v; retrying in %v", err, tempDelay)
				time.Sleep(tempDelay)
				continue
			}
			logrus.Errorf("connection handling failed: %v", err)
			l.Close()
			return
		}
		transport := api.Transport{
			Name:          "mqtt-tcp-listener",
			Channel:       c,
			Encrypted:     false,
			Protocol:      "mqtt",
			RemoteAddress: c.RemoteAddr().String(),
		}
		logrus.Infof("accepted new connection from %s", transport.RemoteAddress)
		go func() {
			err := api.Run(h.broker, transport, tracing.Instance())
			if err != nil {
				logrus.Errorf("failure in connection from %s: %v", transport.RemoteAddress, err)
			} else {
				logrus.Infof("terminated connection from %s", transport.RemoteAddress)
			}
		}()
	}
}
