package main

import (
	"context"
	"log"
	"net/http"
	_ "net/http/pprof"
	"os"
	"os/signal"
	"syscall"

	"github.com/sirupsen/logrus"
	"github.com/vx-labs/iot-mqtt-tcp-listener/listener"
)

func main() {
	logger := logrus.New()
	if os.Getenv("API_ENABLE_PROFILING") == "true" {
		go func() {
			logger.Println(http.ListenAndServe(":8080", nil))
		}()
	}
	mqtt := listener.NewListener(context.Background())
	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)
	go func() {
		mqtt.StartTCP(1883)
	}()
	go serveHTTPHealth()
	<-sigc
	logger.Infof("received interruption: closing broker")
	mqtt.Close()
}

func serveHTTPHealth() {
	mux := http.NewServeMux()
	mux.HandleFunc("/health", func(w http.ResponseWriter, _ *http.Request) {
		w.WriteHeader(http.StatusOK)
	})
	log.Println(http.ListenAndServe("[::]:9000", mux))
}
